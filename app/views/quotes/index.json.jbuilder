json.array!(@quotes) do |quote|
  json.extract! quote, :id, :customer_id, :employee_id, :inventory_id, :status, :date_made
  json.url quote_url(quote, format: :json)
end
