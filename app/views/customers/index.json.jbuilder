json.array!(@customers) do |customer|
  json.extract! customer, :id, :last_name, :first_name, :phone_number, :address, :city, :state, :zip, :status, :date_added
  json.url customer_url(customer, format: :json)
end
