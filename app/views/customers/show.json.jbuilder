json.extract! @customer, :id, :last_name, :first_name, :phone_number, :address, :city, :state, :zip, :status, :date_added, :created_at, :updated_at
