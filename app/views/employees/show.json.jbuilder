json.extract! @employee, :id, :last_name, :first_name, :phone_number, :address, :city, :state, :zip, :job_title, :created_at, :updated_at
