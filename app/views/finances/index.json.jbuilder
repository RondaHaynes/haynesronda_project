json.array!(@finances) do |finance|
  json.extract! finance, :id, :quote_id, :principal_by_pay, :interest, :date, :interest_rate, :payments
  json.url finance_url(finance, format: :json)
end
