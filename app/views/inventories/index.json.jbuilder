json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :make, :model, :color, :vin, :year, :wholesale_price, :retail_price, :on_hand_quantity
  json.url inventory_url(inventory, format: :json)
end
