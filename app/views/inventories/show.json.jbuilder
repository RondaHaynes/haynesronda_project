json.extract! @inventory, :id, :make, :model, :color, :vin, :year, :wholesale_price, :retail_price, :on_hand_quantity, :created_at, :updated_at
