class Quote < ActiveRecord::Base
  belongs_to :customer
  belongs_to :employee
  belongs_to :inventory
  has_many :finances

  def quote_info
    "#{customer_id}, #{inventory_id}"
  end
end
