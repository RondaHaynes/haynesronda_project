class Customer < ActiveRecord::Base
  has_many :quotes
#defines what I will have my quotes page pulled.
  def cus_info
    "#{last_name}, #{first_name}"
  end
  #defines search and calls for results to be pulled when the search contains keyword like the last_name or first_name
  def self.search(search)
    where ['last_name LIKE ? OR first_name LIKE ?', "%#{search}%", "%#{search}%" ]
    end
  end



