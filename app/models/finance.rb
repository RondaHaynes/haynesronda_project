class Finance < ActiveRecord::Base
  belongs_to :quote

  def self.calculate_loans_table(principal_by_pay = 0,payments = 0,interest_rates = 0)
    Rails.logger.debug "Inputs: #{principal_by_pay} #{payments} #{interest_rates}"
    financial = []
    (0..payments).each do |t|
      financial << interest_rates*principal_by_pay/((payments*12)*(1-(1+(interest_rates/payments*12))**-(payments*12)))
    end
    return financial # the word return is optional - could just put population
  end
  def calculate_loans_table
    return self.class.calculate_loans_table(self.principal_by_pay,self.payments,self.interest_rate)
  end


end
