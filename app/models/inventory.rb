class Inventory < ActiveRecord::Base
  has_many :quotes
  #defines what I will have my quotes page pulled.
  def car
    "#{make}, #{model}, #{color}, #{year}, #{retail_price}"
  end
  #defines search and calls for results to be pulled when the search contains keyword like a particular model, color, or vin.
  def self.search(search)
    where ['model LIKE ? OR color LIKE ?  OR vin LIKE ?', "%#{search}%", "%#{search}%", "%#{search}%" ]
  end
end
