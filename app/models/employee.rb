class Employee < ActiveRecord::Base
  has_many :quotes
  #defines what I will have my quotes page pulled.
  def emp_info
    "#{last_name}, #{first_name}"
  end
end
