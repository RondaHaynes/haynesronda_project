class CreateFinances < ActiveRecord::Migration
  def change
    create_table :finances do |t|
      t.integer :quote_id
      t.decimal :principal_by_pay
      t.decimal :interest
      t.date :date
      t.float :interest_rate
      t.decimal :payments

      t.timestamps null: false
    end
  end
end
