class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :customer_id
      t.integer :employee_id
      t.integer :inventory_id
      t.string :status
      t.date :date_made

      t.timestamps null: false
    end
  end
end
