class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.string :make
      t.string :model
      t.string :color
      t.string :vin
      t.integer :year
      t.decimal :wholesale_price
      t.decimal :retail_price
      t.integer :on_hand_quantity

      t.timestamps null: false
    end
  end
end
