# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Inventory.create(make: "Ford",model: "Mustang",color: "Red",year: '2010', vin: "2HNYD28478H501589",wholesale_price: '4000.00', retail_price: '8000.00', on_hand_quantity: '5')
Inventory.create(make: "Honda",model: "Accord",color: "Pink",year: '2008', vin: "3NDHS782615J762892", wholesale_price: '5000.00', retail_price: '10000.00', on_hand_quantity: '5')
Inventory.create(make: "Toyota",model: "Corolla",color: "Black",year: '2000', vin: "6ROND93857K903723", wholesale_price: '2500.00', retail_price: '5000.00', on_hand_quantity: '3')
Inventory.create(make: "Chevy",model: "Camaro",color: "Blue",year: '2015', vin: "8ELIS12345O647589", wholesale_price: '10000.00', retail_price: '20000.00', on_hand_quantity: '6')
Inventory.create(make: "BMW",model: "328i/M3 ",color:"Orange",year: '1999', vin: "1AARO46573N839432", wholesale_price: '3000.00', retail_price: '4500.00', on_hand_quantity: '2')
Inventory.create(make: "Lexus",model: "SC300",color:"Yellow",year: '1996', vin: "9VICT98765R345678", wholesale_price: '2000.00', retail_price: '9000.00', on_hand_quantity: '5')
Inventory.create(make: "Ford",model: "Thunderbird",color: "Blue", year: '1965', vin: "7DEVA56483I957432", wholesale_price: '5000.00', retail_price: '7000.00', on_hand_quantity: '5')
Inventory.create(make: "Land Rover",model: "Defender 90", color: "Violet", year: '1997', vin: "8MISS83647F829462", wholesale_price: '8000.00', retail_price: '9900.00', on_hand_quantity: '5')
Inventory.create(make: "Lincoln",model: "MKZ",color: "Red", year: '2014', vin: "6SHEI32167D453345", wholesale_price: '3000.00', retail_price: '9000.00', on_hand_quantity: '5')